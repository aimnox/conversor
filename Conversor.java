
public static class Conversor {
    public static double convertir(double value, String from, String to) {
        String velocitats = { "KMH", "MS", "MIH", "YDS" };
        if (!(velocitats.contains(from) && velocitats.contains(to))) {
            // Invalid unit
            throw new IllegalArgumentException();
        }
        value= convertTime(value, from.charAt(from.length()-1), to.charAt(from.length()-1));
        value=convertDistance(value, from.substring(0, 2), to.substring(0, 2));

    }

    private static double convertTime(double value, char from, char to) {
        if (from == 'S' && to == 'H') {
            value *= 3600;
        } else if (from == 'H' && to == 'S') {
            value /= 3600;
        }
        return value;
    }

    private static double convertDistance(double value, String from, String to) {
        switch (from) {
        case "KM":
            switch (to) {
            case "M":
                value *= 1000;
                break;
            case "YD":
                value *= 1093.6133;
                break;
            case "MI":
                value *= 0.621371192;
                break;
            default:
                throw new IllegalArgumentException();
                break;
            }
            break;

        case "M":
            switch (to) {
            case "KM":
                value /= 1000;
                break;
            case "YD":
                value *= 1.0936133;
                break;
            case "MI":
                value *= 0.000621371192;
                break;
            default:
                throw new IllegalArgumentException();
                break;
            }
            break;

        case "YD":
            switch (to) {
            case "M":
                value /= 1.0936133;
                break;
            case "KM":
                value /= 1093.6133;
                break;
            case "MI":
                value *= 1760;
                break;
            default:
                throw new IllegalArgumentException();
                break;
            }
            break;
        case "MI":
            switch (to) {
            case "M":
                value *= 1609.344;
                break;
            case "KM":
                value *= 1.609344;
                break;
            case "YD":
                value /= 1760;
                break;
            default:
                throw new IllegalArgumentException();
                break;
            }
            break;
        default:
        throw new IllegalArgumentException();
            break;
        }

        return value;
    }

}